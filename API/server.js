import app from './app.js';

app.listen(5000, () => {
    console.log('Server listening on http://localhost:5000');
});