import express from 'express';
import cors from 'cors';
import itemsRouter from './routes/itemsRouter.js';
import favouriteRouter from './routes/favouriteRouter.js';
import cartRouter from './routes/cartRouter.js';

const app = express();

app.use(cors());
app.use(express.json());
app.use(express.static('static'))
app.use('/items', itemsRouter);
app.use('/favourites', favouriteRouter);
app.use('/cart', cartRouter);

export default app;