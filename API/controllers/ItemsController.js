import items from "../data/items.js";

class ItemsController {
  timeout = 2000;

  getItems = (req, res) => {
    setTimeout(
      () => res.json(items),
      this.timeout,
    )
  }
}

export default ItemsController;
