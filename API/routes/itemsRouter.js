import Router from 'express';
import ItemsController from "../controllers/ItemsController.js";

const itemsRouter = Router();
const itemsController = new ItemsController();

itemsRouter.get('/', itemsController.getItems);

export default itemsRouter;