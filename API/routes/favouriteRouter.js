import { Router } from "express";
import FavouriteController from "../controllers/FavouriteController.js";

const favouriteRouter = Router();
const favouriteController = new FavouriteController();


favouriteRouter.get('/', favouriteController.getFavourites);
favouriteRouter.post('/', favouriteController.addToFavourites);
favouriteRouter.delete('/:id', favouriteController.removeFromFavourites);
export default favouriteRouter;