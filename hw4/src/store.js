import { configureStore } from '@reduxjs/toolkit';
import productsReducer from './redux/productsSlice';
import favouritesReducer from './redux/favouritesSlice';
import modalReducer from "./redux/modalSlice";
import cartReducer from "./redux/cartSlice";



const store = configureStore({
  reducer: {
    products: productsReducer,
    favourites: favouritesReducer,
    modal: modalReducer,
    cart: cartReducer,
  },
})

export default store;