

import { useSelector } from "react-redux";
import NoItemsInFavourites from "../../components/NoItemsInFavourites";

import ProductsContainer from "../../components/ProductsContainer";

const Favourites = () => {
  const favourites = useSelector((state) => state.favourites.data);
  const favouritesLength = useSelector((state) => state.favourites.data.length);
  const isLoading = useSelector((state) => state.favourites.isLoading);
  const isError = useSelector((state) => state.favourites.isError);

  return (
    <>
      {favouritesLength >= 1 && (
        <ProductsContainer
          products={favourites}
          isLoading={isLoading}
          isError={isError}
        />
      )}

      {favouritesLength === 0 && <NoItemsInFavourites />}
    </>
  );
};

export default Favourites;

