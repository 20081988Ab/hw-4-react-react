
import { useContext } from 'react';
import ProductsContainer from '../../components/ProductsContainer';
import { useSelector } from 'react-redux';
import classNames from 'classnames';



const HomePage = () => {

  const isLoading = useSelector(state => state.products.isLoading)
  const isError = useSelector(state => state.products.isError)
  const products = useSelector((state) => state.products.data);


  return (
    <>

      <h1>Home page</h1>
      <ProductsContainer products={products} isError={isError} isLoading={isLoading} />

    </>

  );
};
export default HomePage;