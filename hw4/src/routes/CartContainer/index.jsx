import styles from "./CartContainer.module.scss";
import { useSelector } from "react-redux";
import CartItem from "../../components/CartItem";
import NoItemsInCart from "../../components/NoItemsInCart"
import CartForm from "../../components/CartForm";

const CartContainer = () => {
  const cart = useSelector((state) => state.cart.data);
  const cartLength = useSelector(state => state.cart.data.length);
  const isLoading = useSelector((state) => state.cart.isLoading);
  const isError = useSelector((state) => state.cart.isError);

  if (isLoading) {
    return (
      <div className={styles.container}>
        <h1>Loading...</h1>
      </div>
    );
  }

  if (isError) {
    return (
      <div className={styles.container}>
        <h1>Something goes wrong.</h1>
      </div>
    );
  }

  
  return (
    <>


      {cartLength >= 1 && (
        <div className={styles.cartContainer}>
          {cart.map((cartItem) => (
            <CartItem product={cartItem} key={cartItem.id} />
          ))}
        </div>
      )}

      {cartLength === 0 && <NoItemsInCart />}

      <h2>Fill up the form</h2>
      <CartForm cart={cart} />

    </>
  );
};


export default CartContainer;
