import { createSlice, createAsyncThunk } from '@reduxjs/toolkit';
import axios from 'axios';

export const fetchAllCartItems = createAsyncThunk(
    'cart/fetchAllCartItems',
    async () => {
        const { data } = await axios.get('http://localhost:5000/cart');
        return data;
    }
);

export const addToCart = createAsyncThunk(
    'cart/addToCart',
    async (id) => {
        const { data } = await axios.post('http://localhost:5000/cart',
            { id });
        return data;
    }
);

export const decrementCartItemQuantity = createAsyncThunk(
    'cart/decrementCartItemQuantity',
    async (id) => {
        const { data } = await axios.patch(`http://localhost:5000/cart/${id}`);
        return data;
    }
);

export const deleteFromCart = createAsyncThunk(
    'cart/deleteFromCart',
    async (id) => {
        const { data } = await axios.delete(`http://localhost:5000/cart/${id}`);
        return data;
    }
);

const initialState = {
    data: [],
    isLoading: false,
    isError: false,
    isCartItem: true,
}

export const cartSlice = createSlice({
    name: 'cart',
    initialState,
    reducers: {},
    extraReducers: (builder) => {

        //------FETCHING ALL CART--------------------------------

        builder.addCase(fetchAllCartItems.fulfilled, (state, action) => {
            state.data = action.payload.map(el => ({ ...el, addedToCart: true }));
            state.isLoading = false;
            state.isError = false;
        })

        builder.addCase(fetchAllCartItems.pending, (state) => {
            state.isLoading = true;
        })

        builder.addCase(fetchAllCartItems.rejected, (state) => {
            state.isLoading = false;
            state.isError = true;
        })

//         //------ADD TO CART--------------------------------

        builder.addCase(addToCart.fulfilled, (state, action) => {
            const index = state.data.findIndex(el => el.id === action.payload.id);
            if (index === -1) {
                state.data.push({ ...action.payload, addedToCart: true })
            } else {
                state.data[index].quantity += 1;
            }

        })

//         //------DECREMENT QUANTITY--------------------------------

        builder.addCase(decrementCartItemQuantity.fulfilled, (state, action) => {
            const index = state.data.findIndex(el => el.id === action.payload.id);
            if (index !== -1) {

                if (state.data[index].quantity === 1) {
                    return;
                } else {
                    state.data[index].quantity -= 1;
                }
            }
        })

//         //------REMOVE FROM CART--------------------------------

        builder.addCase(deleteFromCart.fulfilled, (state, action) => {
            const index = state.data.findIndex(el => el.id === action.meta.arg);
            if (index !== -1) state.data.splice(index, 1);
            
        })
    },


})


// // export const { countCartItems } = cartSlice.actions;

export default cartSlice.reducer
