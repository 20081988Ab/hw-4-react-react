
import { createSlice } from '@reduxjs/toolkit';


const initialState = {
  isOpen: false,
  data: {},
  title: '',
  actions: null,
}

export const modalSlice = createSlice({
  name: 'modal',
  initialState,
  reducers: {
    openModal: (state, action) => {
      state.isOpen = true;
      state.data = action.payload;
    },
    closeModal: state => {
      state.isOpen = false;
    },
    setModalTitle: (state, action) => {
      state.title = action.payload.title;
    },
    setModalActions: (state, action) => {
      state.actions = action.payload;
    }
  },
})


export const { openModal, closeModal, setModalTitle, setModalActions } = modalSlice.actions;

export default modalSlice.reducer
