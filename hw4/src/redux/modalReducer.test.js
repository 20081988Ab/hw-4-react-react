import modalReducer from "./modalSlice";

describe('modalReducer works', () => {
    test('should modalReducer open modal', () => {
        const state = { isOpen: false }
        const action = { type: 'modal/openModal' };

        expect(modalReducer(state, action)).toEqual({ isOpen: true })
    });


    test('should modalReducer close modal', () => {
        const state = { isOpen: true }
        const action = { type: 'modal/closeModal' };

        expect(modalReducer(state, action)).toEqual({ isOpen: false })
    });

});




