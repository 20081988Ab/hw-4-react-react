
import { createAsyncThunk, createSlice } from '@reduxjs/toolkit'
import axios from "axios";

export const fetchAllProducts = createAsyncThunk(
  "products/fetchAllProducts",
  async () => {
    const { data } = await axios.get("http://localhost:5000/items");
    return data;
  }

)

const initialState = {
  data: [],
  isLoading: false,
  isError: false,
}

export const productsSlice = createSlice({
  name: 'products',
  initialState,
  reducers: {},
  extraReducers: (builder) => {

    builder.addCase(fetchAllProducts.fulfilled, (state, action) => { 
      state.data = action.payload;
      state.isLoading = false;
      state.isError = false;
    })

    builder.addCase(fetchAllProducts.pending, (state) => { 
      state.isLoading = true;
    })

    builder.addCase(fetchAllProducts.rejected, (state) => { 
      state.isLoading = false;
      state.isError = true;
      
    })

  }

})


export default productsSlice.reducer
