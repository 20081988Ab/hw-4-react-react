
import { createAsyncThunk, createSlice } from '@reduxjs/toolkit'
import axios from "axios";

export const fetchFavourites = createAsyncThunk(
  "favourites/fetchFavourites",
  async () => {
    const { data } = await axios.get("http://localhost:5000/favourites");
    return data;
  }

);

export const addToFavourites = createAsyncThunk(
  'favourites/addToFavourites',
  async (id) => {
      const { data } = await axios.post('http://localhost:5000/favourites',
          { id });
      return data;
  }
);


export const removeFromFavourites = createAsyncThunk(
  "favourites/removeFromFavourites",
  async (id) => {
    const { data } = await axios.delete(`http://localhost:5000/favourites/${id}`);
    return data;

  }
);

const initialState = {
  data: [],
  isLoading: false,
  isError: false,
}



export const favouritesSlice = createSlice({
  name: 'favourites',
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder.addCase(fetchFavourites.fulfilled, (state, action) => {
      state.data = action.payload.map(el => ({ ...el, isFavourite: true }));
      state.isLoading = false;
      state.isError = false;

    })

    builder.addCase(fetchFavourites.pending, (state) => {
      state.isLoading = true;
    })

    builder.addCase(fetchFavourites.rejected, (state) => {
      state.isLoading = false;
      state.isError = true;

    })

    builder.addCase(addToFavourites.fulfilled, (state, action) => {
      state.data.push({ ...action.payload.item, isFavourite: true });
      state.isLoading = false;


    })

    builder.addCase(removeFromFavourites.fulfilled, (state, action) => {
      const index = state.data.findIndex(todo => todo.id === action.payload.item.id)
      if (index !== -1) state.data.splice(index, 1)
    })

  }

})

export default favouritesSlice.reducer
