import styles from "./ProductCard.module.scss";
import StarSvg from "../svg/StarSvg";
import CartIcon from "../svg/CartIcon"
import classNames from "classnames";
import { useDispatch } from "react-redux";
import PropTypes from "prop-types";
import { addToFavourites } from "../../redux/favouritesSlice";
import { removeFromFavourites } from "../../redux/favouritesSlice";
import { openModal, setModalTitle, setModalActions } from "../../redux/modalSlice";
import ProductsContext from "../../context/ProductsContext"
import { useContext } from "react";

const ProductCard = ({ product = {} }) => {
  const { isCard } = useContext(ProductsContext)

  const dispatch = useDispatch();
  const { id, img = "", name = "Product you can buy", price = "100", isFavourite = false, } = product;

  const handleFavourites = () => {
    if (!isFavourite) {
      dispatch(addToFavourites(id));

    } else {
      dispatch(removeFromFavourites(id));

    }
  };



  return (
    <div className={classNames(styles.card, {
      [styles.listView]: !isCard
    })}>

      <div
        className={classNames(styles.image, {
          [styles.listImage]: !isCard
        })}
        style={{ backgroundImage: `url(${img})` }}
      ></div>

      <div className={styles.productInfo}>
        <p className={styles.productName}>{name}</p>
        <p className={classNames(styles.productPrice, {
          [styles.listPrice]: !isCard
        })}>₴ {price}</p>
      </div>

      <div className={styles.btnContainer}>
        <button className={classNames(styles.btn)} onClick={handleFavourites}><StarSvg placedOnCard={true} isFavourite={isFavourite} /></button>
        <button className={classNames(styles.btn, styles.cartBtn)} onClick={() => {
          dispatch(openModal(product));
          dispatch(setModalTitle({ title: ` Add ${name} to cart?` }));
          dispatch(setModalActions('addToCart'));
        }}><CartIcon /></button>
      </div>
    </div>
  )

};

ProductCard.propTypes = {
  product: PropTypes.shape({
    id: PropTypes.string.isRequired,
    img: PropTypes.string,
    name: PropTypes.string,
    price: PropTypes.number,
    isFavourite: PropTypes.bool,
  }),
};


export default ProductCard;


