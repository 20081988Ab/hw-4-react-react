import { NavLink } from "react-router-dom";
import styles from "./HomeSvg.module.scss";
import classnames from "classnames";

const HomeSvg = () => {
  return (
    <NavLink
      to="/"
      className={({ isActive }) =>
        classnames(styles.homePage, {
          [styles.active]: isActive,
        })
      }
    >
      <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
        <path d="M10,20V14H14V20H19V12H22L12,3L2,12H5V20H10Z" />
      </svg>
    </NavLink>
  );
};

export default HomeSvg;
