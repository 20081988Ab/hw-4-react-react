import styles from "./NoItemsInCart.module.scss";

const NoItemsInCart = () => {
  return (
    <h1 className={styles.noItemsNotification}>
      No items in the cart!
    </h1>
  );
};

export default NoItemsInCart;
