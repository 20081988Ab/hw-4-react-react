import "@testing-library/jest-dom";
import Modal from ".";
import { render, screen, fireEvent } from "@testing-library/react";
import store from "../../store"
import { Provider, useDispatch } from "react-redux";
import { openModal, closeModal } from "../../redux/modalSlice";

const Wrapper = () => {
    const dispatch = useDispatch();
    return (
        <>
            <button onClick={() => { store.dispatch(openModal()) }}>open modal</button>
            <Modal />
        </>
    )
}

describe('Modal open/close', () => {
    test('should render on open', () => {
        render(
            <Provider store={store}>
                <Wrapper />
            </Provider>
        )

        const openBtn = screen.getByText('open modal');
        fireEvent.click(openBtn);
        screen.debug()
        expect(screen.getByTestId('modal')).toBeInTheDocument();
    });
});

const CloseWrapper = () => {
    const dispatch = useDispatch();
    return (
        <>
            <button onClick={() => { store.dispatch(closeModal()) }}>close modal</button>
            <Modal />
        </>
    )
}

describe('Modal close', () => {
    test('should modal close on close', () => {
        render(
            <Provider store={store}>
                <CloseWrapper />
            </Provider>
        )

        const closeBtn = screen.getByText('close modal');
        fireEvent.click(closeBtn);
        expect(screen.queryByTestId('modal')).not.toBeInTheDocument();
    });
});

