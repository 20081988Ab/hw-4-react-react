import styles from "./Modal.module.scss";
import { useSelector, useDispatch } from "react-redux";
import { closeModal } from "../../redux/modalSlice";
import { addToCart } from "../../redux/cartSlice";
import { deleteFromCart } from "../../redux/cartSlice";


const Modal = () => {

  const dispatch = useDispatch();

  const { isOpen, title, actions } = useSelector((state) => state.modal);
  const { img, id, name } = useSelector((state) => state.modal.data || {});

  if (!isOpen) {
    return null;
  }

  return (
    <div className={styles.root} data-testid='modal'>
      <div
        className={styles.background}
        onClick={() => {
          dispatch(closeModal());
        }}
      />
      <div className={styles.contentContainer}>
        <h2>{title}</h2>
        {img && <img src={img} alt="image" />}
        {/* <img src={img} alt="image" /> */}

        <p>{name}</p>

        <div className={styles.buttonsContainer}>
          <button
            className={styles.btn}
            onClick={() => {
              if (actions === "addToCart") {
                dispatch(addToCart(id));
              } else if (actions === "deleteCartItem") {
                dispatch(deleteFromCart(id));
              }
              dispatch(closeModal());
            }}
          >
            Yes
          </button>

          <button
            className={styles.btn}
            onClick={() => {
              dispatch(closeModal());
            }}
          >
            No
          </button>
        </div>
      </div>
    </div>
  );
};

export default Modal;
