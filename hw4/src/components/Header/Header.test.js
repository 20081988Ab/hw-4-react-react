import { BrowserRouter } from "react-router-dom";
import { Provider } from "react-redux";
import Header from ".";
import { render} from "@testing-library/react";
import store from "../../store"

describe('Header snapshot testing', () => {
    test('should Header render', () => {
        const { asFragment } = render(<Provider store={store}>
            <BrowserRouter><Header /></BrowserRouter>
        </Provider>)
        expect(asFragment()).toMatchSnapshot();
    });
});