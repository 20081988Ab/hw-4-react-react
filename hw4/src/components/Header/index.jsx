import styles from "./Header.module.scss";
import CartSvg from "../svg/CartSvg";
import IsFavouriteCounter from "../IsFavouriteCounter";
import HomeSvg from "../svg/HomeSvg";
import { useContext } from "react";
import classNames from "classnames";
import ProductsContext from "../../context/ProductsContext";
import CardSvg from "../../assets/CardsSvg"
import ListSvg from "../../assets/ListSvg"



const Header = () => {

  const { isCard, setIsCard } = useContext(ProductsContext);
  console.log(isCard)

  return (
    <header className={classNames(styles.header,

    )}>

      <div className={styles.container}>
        <a className={styles.shopName} href="#">
          My amazing shop
        </a>

        <nav className={styles.cartContainer}>
          <HomeSvg />
          <IsFavouriteCounter />
          <CartSvg />
        </nav>

        <button onClick={() => { setIsCard(prev => !prev) }}
        >{isCard ? <CardSvg /> : <ListSvg />}
        </button>

      </div>
    </header>
  );
};

export default Header;
