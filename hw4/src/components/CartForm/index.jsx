import { Formik, Form, Field } from "formik";
import CustomInput from "../../components/CustomInput";
import { validationSchema } from "./validationSchema"

const CartForm = ({ cart }) => {

    const initialValues = {
        name: "",
        lastName: "",
        age: "",
        adress: "",
        phone: "",
    }
    let selectedItems = [];
    cart.forEach((el) => {
        selectedItems.push(el.name)
       
       
    })
   

    const onSubmit = (values, { resetForm }) => {
        console.log(values)
        console.log("Selected items:", selectedItems)
        resetForm()
    }

    return (
        <Formik initialValues={initialValues} onSubmit={onSubmit} validationSchema={validationSchema}>
            <Form>

                <CustomInput type="text" label="Name *" placeholder="First Name" name="name" />
                <CustomInput type="text" label="Last Name *" placeholder="Last Name" name="lastName" />
                <CustomInput type="text" label="Your Age *" placeholder="Your Age" name="age" />
                <CustomInput type="text" label="Your Adress *" placeholder="Your Adress" name="adress" />
                <CustomInput type="text" label="Your Phone *" placeholder="Your Phone" name="phone" />

                <button type="submit">Buy</button>

            </Form>
        </Formik >
    )
}

export default CartForm