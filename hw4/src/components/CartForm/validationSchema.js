import * as Yup from "yup";

export const validationSchema = Yup.object().shape({
    name: Yup.string()
        .required('Required')
        .min(2, "Too Short!")
        .max(20, "Too Long!"),

    lastName: Yup.string()
        .required('Required')
        .min(2, "Too Short!")
        .max(20, "Too Long!"),

    age: Yup.number()
        .min(1, "Age should be more then 1!")
        .max(110, "Age should  be less then 110!")
        .integer("Age should be integer")
        .required('Required'),



    adress: Yup.string()
        .required('Required')
        .min(2, "Too Short!")
        .max(100, "Too Long!"),

        phone: Yup.number()
       .required('Required'),
});