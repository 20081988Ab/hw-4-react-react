
import PropTypes from "prop-types";
import { openModal, setModalText } from "../../redux/modalSlice";
import { useDispatch } from "react-redux";

const Button = ({type = "button" }) => {
  const dispatch = useDispatch()
  return (
    <button onClick={() => {
      dispatch(setModalText({description:"Add to cart?" }))
       dispatch(openModal()) }} 
       type={type}>Add to cart
    </button>
  );
};


Button.propTypes = {
  children: PropTypes.node,
  type: PropTypes.oneOf(['button', 'submit', 'reset']),
};

export default Button;


