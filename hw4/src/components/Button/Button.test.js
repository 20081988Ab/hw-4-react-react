import Button from ".";
import { render, screen, fireEvent } from "@testing-library/react";

describe('Button snapshot testing', () => {
    test('should Button render', () => {

        // перевірила що кнопка рендериться снепшотом
        const { asFragment } = render(<button>Add to cart</button>)

        expect(asFragment()).toMatchSnapshot();
    });
});

describe('Button onClick works', () => {
    test('should onClick works', () => {

        // створила функцію jest.fn
        const callback = jest.fn()
        // render кнопки
        render(<button onClick={callback}>Add to cart</button>);
        // знайшла цю кнопку - screen.getByText
        const btn = screen.getByText('Add to cart');
        // клік на кнопку 
        fireEvent.click(btn);
        // перевірка чи був клік на кнопку 
        expect(callback).toBeCalled()
    });
});