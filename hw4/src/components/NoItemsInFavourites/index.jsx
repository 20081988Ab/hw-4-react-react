import styles from "../NoItemsInFavourites";

const NoItemsInFavourites = () => {
  return (
    <h1 className={styles.noItemsNotification}>
      There is no items
    </h1>
  );
};

export default NoItemsInFavourites;
