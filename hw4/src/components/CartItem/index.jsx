import styles from "./CartItem.module.scss";
import DeleteSvg from "../svg/DeleteSvg";

import PropTypes from "prop-types";
import { useDispatch } from "react-redux";

import { decrementCartItemQuantity } from "../../redux/cartSlice";
import { openModal, setModalTitle, setModalActions } from "../../redux/modalSlice";

const CartItem = ({ product }) => {
  const { id, img, name, price, quantity } = product;

  const dispatch = useDispatch();

  const handleDecrementItemsQuantity = () => {
    dispatch(decrementCartItemQuantity(id));
  };

  const totalPrice = price * quantity;

  return (
    <div className={styles.itemContainer}>
      <img src={img} className={styles.image} alt="" />

      <div className={styles.addInfo}>
        <span>{name}</span>

        <span>Price: {price} ₴ </span>
      </div>

      <div className={styles.price}>
        <div className={styles.quantity}>
          <span>Quantity: {quantity} </span>

          <div
            className={styles.countButtonsContainer}
            onClick={handleDecrementItemsQuantity}
          >
            <button 
            className={styles.countButton}
            disabled={quantity === 1}
            >-</button>
          </div>
        </div>

        <span>Total price: {totalPrice} ₴ </span>
      </div>

      <div
        className={styles.buttonContainer}
        onClick={() => {
          dispatch(openModal(product));
          dispatch(setModalTitle({ title: `Delete ${name}?` }));
          dispatch(setModalActions("deleteCartItem"));
        }}
      >
        <button className={styles.deleteButton}>
          <DeleteSvg />
        </button>
      </div>
    </div>
  );
};

CartItem.propTypes = {
  product: PropTypes.shape({
    id: PropTypes.string.isRequired,
    img: PropTypes.string,
    name: PropTypes.string,
    price: PropTypes.number,
    quantity: PropTypes.number,
  }),
};

export default CartItem;
