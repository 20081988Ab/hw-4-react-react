
import { useSelector } from "react-redux";
import styles from "./IsFavouriteCounter.module.scss";
import { NavLink } from "react-router-dom";
import classnames from "classnames";

import StarSvg from "../svg/StarSvg";

const IsFavouriteCounter = () => {

  const isFavouriteCounter = useSelector(state => state.favourites.data.length);

  return (
    <NavLink
      to="/favourites"
      className={({ isActive }) =>
        classnames(styles.IsFavouriteCounterContainer, {
          [styles.active]: isActive,
        })
      }
    >
      <StarSvg/>

      <p className={styles.counter}>{isFavouriteCounter}</p>
    </NavLink>
  );
};

export default IsFavouriteCounter;
