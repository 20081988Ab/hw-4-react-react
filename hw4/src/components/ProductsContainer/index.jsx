import PropTypes from "prop-types";
import ProductCard from "../ProductCard";
import styles from "./ProductsContainer.module.scss";


const ProductsContainer = ({ products = [], isLoading = false, isError = false }) => {

  if (isLoading) {
    return (
      <div className={styles.container}>
        <h1>Loading.....</h1>
      </div>
    )
  }


  
  if (isError) {
    return (
      <div className={styles.container}>
        <h1>Sorry, we are broken</h1>
        <button onClick={()=>{window.location.reload()}}>Click to reload</button>
      </div>
    )
  }
  return (
    <div className={styles.container}>
      {products.map((product) => (
        <ProductCard key={product.id} product={product} />
      ))}
    </div>
  );
};

ProductsContainer.propTypes = {
  products: PropTypes.array,
  isLoading: PropTypes.bool,
  isError: PropTypes.bool,
};

export default ProductsContainer;
