import { useField } from "formik";
import styles from "./CustomInput.module.scss"
import classNames from 'classnames';
import PropTypes from "prop-types";

const CustomInput = (props) => {
    const { type, placeholder, label } = props;
    const [field, meta] = useField(props);
    const isError = meta.touched && meta.error;
    return (

        <div className={styles.container}>
            <label >
                <p className={styles.label}>{ label }</p>
                <input className={classNames(styles.input, { [styles.highlight]: isError })} type={type} placeholder={placeholder} {...field} />
                {isError && <p className={styles.error}>{meta.error}</p>}
            </label>
        </div>

    )
}
CustomInput.propTypes = {
    type: PropTypes.string.isRequired,
    placeholder: PropTypes.string,
    label: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
  };
  
  export default CustomInput;
  


