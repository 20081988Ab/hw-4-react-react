
import sum from "./index.js"

describe('Sum function works', () => {

    test('should return valid sum', () => {


        expect(sum(2, 5)).toBe(7)
    });


    test('should return valid sum with another values', () => {


        expect(sum(6, 90)).toBe(96)
    });
});



  