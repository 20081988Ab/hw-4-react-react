import isInteger from ".";

describe('iInteger function works', () => {

    test('should return true if integer', () => {
        expect(isInteger(2)).toBe(true)
    });

    test('should return false if non-integer', () => {
        expect(isInteger(0.5)).toBe(false)
    });

});


describe('isInteger function has bad cases', () => {

    test('should return false with bad values', () => {
        expect(isInteger("abcd")).toBe(false)
    })
})