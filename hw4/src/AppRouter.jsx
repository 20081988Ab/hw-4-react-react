import { Routes, Route } from "react-router-dom";
import HomePage from "./routes/HomePage";
import Favourites from "./routes/Favourites";
import CartContainer from "./routes/CartContainer";




const AppRouter = () => {
  return (
    <Routes>

      <Route path="/" element={<HomePage />} />
      <Route path="/favourites" element={<Favourites />} />
      <Route path="/cart" element={<CartContainer />} />

      <Route path="*" element={<h1>404 - PAGE NOT FOUND</h1>} />

    </Routes>
  );
};



export default AppRouter;
