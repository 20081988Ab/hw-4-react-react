import './App.css';
import Header from './components/Header';
import AppRouter from './AppRouter';
import { fetchAllProducts } from './redux/productsSlice';
import { useDispatch } from 'react-redux';
import Modal from "./components/Modal"
import { addToFavourites, fetchFavourites } from './redux/favouritesSlice';
import { addToCart, fetchAllCartItems } from './redux/cartSlice';
import ProductsProvider from './context/ProductsContext/ProductsProvider';


function App() {


  const dispatch = useDispatch();

  dispatch(fetchAllProducts());
  dispatch(fetchFavourites());
  dispatch(addToFavourites());
  dispatch(fetchAllCartItems());
  dispatch(addToCart());
  return (
    <>
      <ProductsProvider>
        <Header />
        <Modal />
        <AppRouter />
      </ProductsProvider>
    </>
  )
}

export default App

