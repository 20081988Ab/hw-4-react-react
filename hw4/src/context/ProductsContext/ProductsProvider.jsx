import { useState } from "react";
import ProductsContext from ".";
import PropTypes from "prop-types";

const ProductsProvider = ({ children }) => {

    const [isCard, setIsCard] = useState(true);

    const value = {
        isCard,
        setIsCard,
    }

    return (
        <ProductsContext.Provider value={value}>
            {children}
        </ProductsContext.Provider>
    )

}

ProductsProvider.propTypes = {
    children: PropTypes.oneOfType([PropTypes.node, PropTypes.arrayOf([PropTypes.node])]),
}

export default ProductsProvider;