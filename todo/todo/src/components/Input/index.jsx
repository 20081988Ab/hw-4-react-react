import { useField } from "formik";
import TextField from '@mui/material/TextField';


const Input = (props) => {

    const [field] = useField(props);

    return (

        <div >
            <TextField maxRows={4} sx={{width:"100%"}} {...field} {...props} />
        </div>

    )
}


export default Input;